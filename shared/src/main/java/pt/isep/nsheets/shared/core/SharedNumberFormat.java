package pt.isep.nsheets.shared.core;

import java.text.ParseException;

import com.google.gwt.core.shared.GWT;
//import java.text.ParsePosition;	// Not supported in GWT
//import java.text.NumberFormat;	// Not supported in GWT

/**
* The purpose of this class is to allow number formatting on both the client and server side.
*/
public class SharedNumberFormat
{
    public static double parse(String value) throws ParseException {
    	// Need to change "," to decimalSeparator...
    	// "," is used in the grammar as a decimal separator
        if(GWT.isClient())
        {   	
        	try {
        		String decimalSeparator = com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().getNumberConstants().decimalSeparator();
        		String value2 = value.replace(",", decimalSeparator);

        		double numberValue = com.google.gwt.i18n.client.NumberFormat.getDecimalFormat().parse(value2);
        		return numberValue;
        	}
        	catch (java.lang.NumberFormatException e) {
        		throw new ParseException(value, 0);
        	}
        }
        else {
    		java.text.ParsePosition position = new java.text.ParsePosition(0);
    		Number number = java.text.NumberFormat.getInstance().parse(value, position);
    		if (position.getIndex() == value.length())
    			return number.doubleValue();
    		throw new ParseException(value, position.getErrorIndex());
        }
    }
}
